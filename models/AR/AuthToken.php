<?php
namespace app\models\AR;

use Yii;
use yii\db\ActiveRecord;

/**
 * @property int $id
 * @property string $email
 * @property string $token
 * @property bool $is_active
 * @property string $creation_date
 */
class AuthToken extends ActiveRecord {

    const TOKEN_LENGTH = 64;

    public static function createByEmail($email) {
        $result = new self();
        $result->email = $email;
        $result->token = static::_generateToken();
        $result->creation_date = (new \DateTime())->format('Y-m-d H:m:s');
        $result->save();
        return $result;
    }

    /**
     * @param string $token
     * @return self
     */
    public static function findActiveToken($token) {
        return static::findOne(['token' => $token, 'is_active' => 1]);
    }

    /**
     * @param string $token
     */
    public static function deactivate($token) {
        $token = static::findOne(['token' => $token]);
        if ($token === null) {
            return;
        }
        $token->is_active = 0;
        $token->save();
    }

    /**
     * @return string
     */
    private static function _generateToken() {
        return Yii::$app->getSecurity()->generateRandomString(self::TOKEN_LENGTH);
    }
}