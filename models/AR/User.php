<?php

namespace app\models\AR;

use Yii;
use yii\db\ActiveRecord;
use yii\rbac\Assignment;

/**
 * @property int $id
 * @property string $name
 * @property string $email
 * @property string $registration_date
 */
class User extends ActiveRecord implements \yii\web\IdentityInterface
{

    /**
     * @param string $email
     * @return self
     */
    public static function createByEmail($email) {
        $result = new self();
        $result->email = $email;
        $result->registration_date = (new \DateTime())->format('Y-m-d H:m:s');
        $result->save();
        return $result;
    }

    /**
     * @param string $email
     * @return bool
     */
    public static function isIdentityWithEmailExists($email) {
        return static::findOne(['email' => $email]) !== null;
    }

    /**
     * @return self[]
     */
    public static function loadAll() {
        return static::find()->all();
    }

    /**
     * @param User $user
     * @return Assignment
     */
    public static function isAdmin(self $user) {
        return Yii::$app->authManager->getAssignment('admin', $user->id);
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id]);
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        $token = AuthToken::findActiveToken($token);
        return $token === null ? null : static::findOne(['email' => $token->email]);
    }

    /**
     * @param string $name
     */
    public function changeName($name) {
        $this->name = $name;
        $this->save();
    }

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * {@inheritdoc}
     */
    public function getAuthKey()
    {
    }

    /**
     * {@inheritdoc}
     */
    public function validateAuthKey($authKey)
    {
    }
}
