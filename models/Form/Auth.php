<?php

namespace app\models\Form;

use Yii;
use yii\base\Model;
use yii\helpers\Url;
use app\models\AR;

class Auth extends Model {

    const TOKEN_LENGTH = 64;

    public $email;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            ['email', 'required'],
            ['email', 'email'],
        ];
    }

    public function processFormSubmit() {
        $authToken = $this->_createAuthToken();
        $this->_sendAuthLink($authToken->token);
    }

    /**
     * @param string $token
     */
    private function _sendAuthLink($token) {
        Yii::$app->mailer->compose()
            ->setTo($this->email)
            ->setFrom(Yii::$app->params['adminEmail'])
            ->setSubject('Ссылка авторизации')
            ->setTextBody($this->_getAuthMailBody($token))
            ->send();
    }

    /**
     * @param string $token
     * @return string
     */
    private function _getAuthMailBody($token) {
        return "Для авторизации перейдите по ссылке: {$this->_getAuthLink($token)}";
    }

    /**
     * @param string $token
     * @return string
     */
    private function _getAuthLink($token) {
        return Url::toRoute(['login', 'email' => $this->email, 'token' => $token], true);
    }

    /**
     * @return AR\AuthToken
     */
    private function _createAuthToken() {
        return AR\AuthToken::createByEmail($this->email);
    }
}