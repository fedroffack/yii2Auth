<?php

namespace app\controllers;

use app\models\AR;
use Yii;
use yii\data\ArrayDataProvider;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use app\models\Form;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'dashboard', 'change-name', 'user-list'],
                'rules' => [
                    [
                        'actions' => ['logout', 'dashboard', 'change-name'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['user-list'],
                        'allow' => true,
                        'roles' => ['admin'],
                    ]
                ],
                'denyCallback' => function () {
                    return $this->redirect(['auth']);
                },
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionIndex()
    {
        return $this->redirect(['dashboard']);
    }

    public function actionAuth()
    {
        $authForm = new Form\Auth();
        $isEmailSent = false;
        if ($authForm->load(Yii::$app->request->post())) {
            $authForm->processFormSubmit();
            $isEmailSent = true;
        }
        return $this->render('auth', ['model' => $authForm, 'isEmailSent' => $isEmailSent]);
    }

    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $request = Yii::$app->request;
        $email = $request->get('email');
        $token = $request->get('token');

        if ($email === null || $token === null) {
            return $this->_notFound();
        }

        if (!AR\User::isIdentityWithEmailExists($email)) {
            AR\User::createByEmail($email);
        }

        if (Yii::$app->user->loginByAccessToken($token) === null) {
            return $this->_notFound();
        };

        AR\AuthToken::deactivate($token);

        return $this->redirect(['dashboard']);
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    public function actionDashboard()
    {
        $user = Yii::$app->user->getIdentity();
        return $this->render('dashboard', ['name' => $user->name]);
    }

    public function actionChangeName()
    {
        $name = Yii::$app->request->post('name');
        $userId = Yii::$app->user->getId();
        $user = AR\User::findIdentity($userId);
        $user->changeName($name);
        return $this->asJson(['success' => true]);
    }

    public function actionUserList()
    {
        $dataProvider = new ArrayDataProvider([
            'allModels' => AR\User::loadAll(),
        ]);
        return $this->render('user_list', ['dataProvider' => $dataProvider]);
    }

    private function _notFound() {
        return $this->render('error', ['name' => 'Не найдено', 'message' => 'Не найдено']);
    }
}
