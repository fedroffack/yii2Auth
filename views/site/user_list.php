<?php

/* @var $dataProvider ArrayDataProvider */

use yii\data\ArrayDataProvider;
use yii\grid\GridView;

echo GridView::widget([
    'dataProvider' => $dataProvider,
]);