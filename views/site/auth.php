<?php

/* @var $this yii\web\View */
/* @var $isEmailSent */

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

?>

<?php if (!$isEmailSent): ?>
    <div class="js-form-container">
        <?php $form = ActiveForm::begin(['id' => 'auth-form']); ?>

            <?= $form->field($model, 'email'); ?>

            <div class="form-group">
                <?= Html::submitButton('Получить ссылку для входа', ['class' => 'js-auth-button btn btn-primary', 'name' => 'auth-button']) ?>
            </div>

        <?php ActiveForm::end(); ?>
    </div>
<?php else: ?>
    <div>Ссылка отправлена на указанную почту</div>
<?php endif; ?>