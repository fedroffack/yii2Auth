<?php

/* @var $this yii\web\View */
/* @var $changeNameResponse string */

use yii\helpers\Html;
use yii\helpers\Url;

$changeNameUrl = Url::to(['/site/change-name']);
?>

<div class="js-user-name-container form-group">
    <span>Ваше имя:</span>
    <input type="text" name="user-name" class="form-control" value="<?= Html::encode($name) ?>">
</div>

<?php

$script = <<<JS
$('.js-user-name-container input').on('change', function (a) {
    const newName = $(this).val();
   $.post('$changeNameUrl', {name: newName}, (response) => {
       if (!response.success) {
           alert('Произошла ошибка');
           return;
       }
       $('.js-navbar-user-name').text(newName);
   });
});
JS;

$this->registerJs($script);

