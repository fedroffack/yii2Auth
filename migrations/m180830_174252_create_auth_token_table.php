<?php

use yii\db\Migration;

/**
 * Handles the creation of table `auth_token`.
 * Has foreign keys to the tables:
 *
 * - `user`
 */
class m180830_174252_create_auth_token_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('auth_token', [
            'id' => $this->primaryKey(),
            'email' => $this->string()->notNull(),
            'token' => $this->string()->notNull(),
            'is_active' => $this->boolean()->notNull()->defaultValue(1),
            'creation_date' => $this->datetime(),
        ]);

        // creates index for column `email`
        $this->createIndex(
            'idx-auth_token-email',
            'auth_token',
            'email'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops index for column `email`
        $this->dropIndex(
            'idx-auth_token-email',
            'auth_token'
        );

        $this->dropTable('auth_token');
    }
}
